﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Distribuciones
{
    public class Generador
    {

        public long m;
        public long a;
        public long c;
        public float ultimo;

        public Generador(long semilla, long m_var, long a_var, long c_var)
        {
            a = a_var;
            m = m_var;
            c = c_var;
            ultimo = semilla;
        }

        public Generador()
        {

        }

        public float Siguiente()
        {
            ultimo = Mixto(ultimo);

            return ultimo / (float)m;
        }

        public float Mixto(float numero)
        {
            return ((a * numero) + c) % m;
        }

    }

    public class Uniforme
    {
        //Generador gen = new Generador(DateTime.Now.Ticks % 4294967296, 4294967296, 1664525, 1013904223);

        Generador gen = new Generador(100, 5000, 17, 43);
        float a;
        float b;

        /// <summary>
        /// Constructor de clase.
        /// </summary>
        /// <param name="a">Límite inferior del intervalo.</param>
        /// <param name="b">Límite superior del intervalo.</param>
        public Uniforme(float a, float b)
        {
            this.a = a;
            this.b = b;
        }

        /// <summary>
        /// Funcion para obtener una variable aleatoria a partir de un número random.
        /// </summary>
        /// <param name="random">Número random entre 0 y 1.</param>
        /// <returns>Variable aleatoria con distribución uniforme.</returns>
        public float ObtenerUniforme(float random)
        {
            return a + random * (b - a);
        }

        /// <summary>
        /// Función para obtener un generador de "n" variables aleatorias con
        /// distribución uniforme.
        /// </summary>
        /// <param name="cantidad_numeros">Cantidad de variables a generar.</param>
        /// <returns>Un IEnumerable de variables aleatorias.</returns>
        public IEnumerable<float[]> ObtenerGeneradorUniforme(long cantidad_numeros)
        {
            float random;
            for (long i = 1; i < cantidad_numeros + 1; i++)
            {
                random = gen.Siguiente();
                yield return new float[] { (float)i, ObtenerUniforme(random) };
            }
        }

    }

    public class ExponencialNegativa
    {
        //Generador gen = new Generador(DateTime.Now.Ticks % 4294967296, 4294967296, 1664525, 1013904223);

        Generador gen = new Generador(100, 5000, 17, 43);
        float lambda;
        float media;


        /// <summary>
        /// Constructor de clase. Si lambda es -1 se setea lambda como (1 / media).
        /// </summary>
        /// <param name="lambda">Parametro de distribución lambda.</param>
        /// <param name="media">Parametro de distribución media.</param>
        public ExponencialNegativa(float lambda, float media)
        {
            if (lambda == -1f)
            {
                this.lambda = 1 / media;
                this.media = media;
            }
            else
            {
                this.lambda = lambda;
            }
        }

        /// <summary>
        /// Funcion para obtener una variable aleatoria a partir de un número random.
        /// </summary>
        /// <param name="random">Número random entre 0 y 1.</param>
        /// <returns>Variable aleatoria con distribución exponencial negativa.</returns>
        public float ObtenerExpNeg(float random)
        {
            return (float)(-1 / this.lambda) * (float)Math.Log(1 - random);
        }

        /// <summary>
        /// Función para obtener un generador de "n" variables aleatorias con
        /// distribución exponencial negativa.
        /// </summary>
        /// <param name="cantidad_numeros">Cantidad de variables a generar.</param>
        /// <returns>Un IEnumerable de variables aleatorias.</returns>
        public IEnumerable<float[]> ObtenerGeneradorExponencialNegativa(long cantidad_numeros)
        {
            float random;
            for (long i = 1; i < cantidad_numeros + 1; i++)
            {
                random = gen.Siguiente();
                yield return new float[] { (float)i, ObtenerExpNeg(random) };
            }
        }

    }

    public class Normal
    {
        //Generador gen = new Generador(DateTime.Now.Ticks % 4294967296, 4294967296, 1664525, 1013904223);

        Generador gen = new Generador(100, 5000, 17, 43);
        float media;
        float desviacion;


        /// <summary>
        /// Constructor de clase.
        /// </summary>
        /// <param name="media">Parametro de distribución media.</param>
        /// <param name="desviacion">Parametro de distribución desviación.</param>
        public Normal(float media, float desviacion)
        {
            this.media = media;
            this.desviacion = desviacion;
        }

        /// <summary>
        /// Funcion para obtener una variable aleatoria a partir de un número random.
        /// </summary>
        /// <param name="random">Número random entre 0 y 1.</param>
        /// <returns>Variable aleatoria con distribución normal.</returns>
        public float ObtenerNormal(float random1, float random2)
        {
            return this.media + ((float)Math.Sqrt((double)(-2 * Math.Log((double)random1))) *
                (float)Math.Cos(2 * Math.PI * random2) * this.desviacion);
        }

        /// <summary>
        /// Función para obtener un generador de "n" variables aleatorias con
        /// distribución normal.
        /// </summary>
        /// <param name="cantidad_numeros">Cantidad de variables a generar.</param>
        /// <returns>Un IEnumerable de variables aleatorias.</returns>
        public IEnumerable<float[]> ObtenerGeneradorNormal(long cantidad_numeros)
        {
            float random_anterior = gen.Siguiente();
            float random_actual;
            float random;
            for (long i = 1; i < cantidad_numeros + 1; i++)
            {
                random_actual = gen.Siguiente();
                random = ObtenerNormal(random_anterior, random_actual);
                random_anterior = random_actual;
                yield return new float[] { (float)i, random };
            }
        }

    }
}
