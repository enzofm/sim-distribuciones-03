﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Distribuciones
{
    public static class Principal
    {
        static void Main(string[] args)
        {
            //Uniforme u = new Uniforme(1f, 100f);
            //foreach (float[] numero in u.ObtenerGeneradorUniforme(50))
            //{
            //    Console.WriteLine(String.Format("{0} -> {1}", numero[0], numero[1].ToString("0.0000")));
            //}

            //ExponencialNegativa en = new ExponencialNegativa(-1f, 5f);
            //foreach (float[] numero in en.ObtenerGeneradorExponencialNegativa(50))
            //{
            //    Console.WriteLine(String.Format("{0} -> {1}", numero[0], numero[1].ToString("0.0000")));
            //}

            Normal n = new Normal(35f, 7f);
            foreach (float[] numero in n.ObtenerGeneradorNormal(50))
            {
                Console.WriteLine(String.Format("{0} -> {1}", numero[0], numero[1].ToString("0.0000")));

            }

        }
    }
}
